var login = require('./login');
var signup = require('./signup');
var fblogin = require('./fblogin');
var glogin = require('./googleLogin')
var User = require('../model/user');

module.exports = function(passport){

	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
      //  console.log('serializing user: ');console.log(user);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
        //    console.log('deserializing user:',user);
            done(err, user);
        });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    fblogin(passport);
    login(passport);
    signup(passport);

}
