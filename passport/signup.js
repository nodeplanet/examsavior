var LocalStrategy   = require('passport-local').Strategy;
var User = require('../model/user');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport){

	passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            findOrCreateUser = function(){
                // find a user in Mongo with provided username
                User.findOne({ 'username' :  username }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err){
                        console.log('Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('User already exists with username: '+username);
                        return done(null, false, req.flash('message','User Already Exists'));
                    } else {
												var email_sign=req.body.email;
                        User.findOne({ email:email_sign },function(err,user){
													if(err){
														console.log('Error in SignUp'+err);
														return done(err);

													}
													if(user){
														console.log('User already exists with email: '+email_sign);
														return done(null, false, req.flash('message','Email Already Exists'));

													}else{
														var newUser = new User();

														// set the user's local credentials
														newUser.username = username;
														newUser.password = createHash(password);
														newUser.email = req.param('email');
														newUser.firstname = req.param('firstname');
														newUser.lastname = req.param('lastname');
														newUser.profilePic="default.png";
														newUser.passRecIndex=req.param('question');
														newUser.PassRecAnswer=req.param('answer');
														// save the user
														newUser.save(function(err) {
																if (err){
																		console.log('Error in Saving user: '+err);
																		throw err;
																}
																console.log('User Registration succesful');
																return done(null, newUser);
																});


															}
												});

											}
									  });
					};
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );

    // Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}








/*
var LocalStrategy   = require('passport-local').Strategy;
var User = require('../model/user');
var bCrypt = require('bcrypt-nodejs');
/*
module.exports = function(passport){
	passport.use('signup', newLocalStrategy({
						passReqToCallback :true  // allows us to pass back the entire request to the callback
        },
				function(req,username,password,done){


						findOrCreateUser= function(){
							if(username.find)


							user.findOne({'username': username}.function(err,user){
								if(err){
									console.log('Error in SignUp: '+err);
									return done(err);
								}
								if(user){
									console.log("User already exists");
									return done(null,false, req.flash('message: user already exists'));
								}


							});//findOne function ends



						}

				}
				//function req,username ends


				})
				//passport.use function ends


module.exports = function(passport){
	passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            findOrCreateUser = function(){
                // find a user in Mongo with provided username
								var email=req.body.email;
								console.log(email);
                User.findOne({ 'username': username }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('User already exists with username: '+username);
                        return done(null, false, req.flash('message','User Already Exists'));
                    }
										else{
											console.log("I am here");
										}
                });
							User.findOne({ 'email' : email }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('User already exists with email: '+username);
                        return done(null, false, req.flash('message','Email Already Exists'));
                    }
                })

            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );

    // Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}

// if there is no user with that email
// create the user
/*
var newUser = new User();

// set the user's local credentials
newUser.username = username;
newUser.password = createHash(password);
newUser.email = req.param('email');
newUser.firstname = req.param('firstname');
newUser.lastname = req.param('lastname');
newUser.profilePic="default.jpg";
// save the user
newUser.save(function(err) {
		if (err){
				console.log('Error in Saving user: '+err);
				throw err;
		}
		console.log('User Registration succesful');
		return done(null, newUser);
});
*/
