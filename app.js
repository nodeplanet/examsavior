var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('es6-promise').polyfill();

var routes = require('./routes/index');
var users = require('./routes/users');
var routes = require('./routes/fireBaseToken');

/* rohan

var mongo = require('mongodb');
  var monk = require('monk');
  var db = monk('localhost:27017/notesharing');
  */

/*var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/notesharing');
var db = mongoose.connection;*/
var passport=require('passport');
var expressSession = require('express-session');
var flash = require('connect-flash');


var initPassport = require('./passport/init');  //initializing passport
var routes = require('./routes/index')(passport);
/*
var users = require('./routes/users');
var login=require('./routes/login')
var mongo=require('./routes/mongo-connection');
*/
var mongoose = require('mongoose');

var url = '127.0.0.1:27017/notestack';
// Connect to DB
//mongoose.connect("mongodb://$OPENSHIFT_MONGODB_DB_HOST:$OPENSHIFT_MONGODB_DB_PORT/notestack");
//mongoose.connect("mongodb://192.168.1.101:27017/test");
//mongoose.connect("OPENSHIFT_MONGODB_DB_URL/notestack");
if (process.env.OPENSHIFT_MONGODB_DB_URL) {
    url = process.env.OPENSHIFT_MONGODB_DB_URL +
    process.env.OPENSHIFT_APP_NAME;
}

// Connect to mongodb
var connect = function () {
    mongoose.connect(url);
};
connect();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
if(process.env.OPENSHIFT_DATA_DIR){
    var loc = process.env.OPENSHIFT_DATA_DIR;
    app.use(express.static(loc));
}
else
  app.use(express.static(path.join(__dirname, 'uploads')));

/* rohan
app.use(function(req,res,next){
  req.db=db;
  next();
}); */



app.use(expressSession({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

initPassport(passport);

app.use('/', routes);
app.use('/users', users);

var routes = require('./routes/index')(passport);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
