var mongoose = require('mongoose');

module.exports = mongoose.model('User',{
	id: String,
	username: String,
	password: String,
	email: String,
	firstname: String,
	lastname: String,
	profilePic: String,
	passRecIndex: Number,
	PassRecAnswer: String,
	facebook         : {
	        id           : String,
	        token        : String,
	        email        : String,
	        firstname	 : String,
	        name         : String,
	        profilePic	 : String
    }	
});