var mongoose = require('mongoose');

module.exports = mongoose.model('comment',{
	id: String,
	comment: String,
	noteid: String,
	userid: String,
	username:String
});
