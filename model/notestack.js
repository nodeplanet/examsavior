var mongoose = require('mongoose');

module.exports = mongoose.model('notestack',{
	id: String,
	notesname: String,
	noteoname: String,
	notesimage: String,
	userid: String

});
