var mongoose = require('mongoose');

module.exports = mongoose.model('notescollection',{
	oname: String,
	sname: String,
	type: String,
	category: String,
	subcategory: String,
	description: String,
	simage: String,
	uploaderid: String,
	uploaderName: String
});