var express = require('express');
var router = express.Router();
var multer = require('multer');
var user = require('../model/user.js');
var notescollection = require('../model/notesdb.js');
var allcomments = require('../model/commentsdb.js');
var notestack = require('../model/notestack.js');
var currentpage;
var recovery_options = ["Select----", "What was your first pet's name", "Name of actor you hate the most"];
var bCrypt = require('bcrypt-nodejs');
require('es6-promise').polyfill();
/* GET home page. */

router.get('/', function (req, res, next) {
  if (req.isAuthenticated()) {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('index',{title:'Home',auth:true,user: req.user});
  }
  else {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('index',{title:'Home',auth:false});
  }
});

router.get('/json',function(req,res,next){

  var availability={
                   userName: 'rohan',
                   status:'always great',
                 };
         res.status(200).json(availability);

})

var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated()){
		return next();

	}
	// if the user is not authenticated then redirect him to the login page
		res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
		res.render('login', { message: req.flash('message'), user: "login", auth:false,title:"Login"});
}
////////////////////   auth token ashu /////

var admin = require("firebase-admin");
var serviceAccount = require("./walltalk-551b1-firebase-adminsdk-47z19-cf8237c32b.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://walltalk-49654.firebaseio.com"
});

router.get('/ashu/authToken/:authToken?',function(req,res) {
  var uid = req.params.authToken;
  admin.auth().createCustomToken(uid)
    .then(function(customToken) {
      res.status(200).json({status:"success",authToken : customToken});
    })
    .catch(function(error) {
      console.log("Error creating custom token:", error);
    });
  
})

/////////////////////// upload ////////////////////////////
///////////  multer  ////////////////

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log('hio in multer dest');
    var type = file.mimetype.split('/');   
    if(process.env.OPENSHIFT_DATA_DIR){
      var loc = process.env.OPENSHIFT_DATA_DIR;
      if(type[1] == 'pdf')
        cb(null, loc +'pdf');
      else if(type[1] == 'x-zip-compressed')
        cb(null, loc +'zip'); 
      else if(type[0] == 'image')
        cb(null, loc + 'images');
      else{
        cb(null,loc +'others');
      }
    }
    else{
      if(type[1] == 'pdf')
        cb(null, __dirname +'/../uploads/pdf');
      else if(type[1] == 'x-zip-compressed')
        cb(null, __dirname +'/../uploads/zip');
      else if(type[0] == 'image')
        cb(null, __dirname+'/../uploads/images');
      else{
        cb(null,__dirname+'/../uploads/others');
      }
    }

  },

  filename: function (req, file, cb) {
    var ext = file.originalname.split(".");
    cb(null, 'a' + Date.now() +'.' +ext[(ext.length)-1] )
  }
});

var filefilter = function(req,file,cb){
  var type = file.mimetype.split('/');
  if(file.fieldname == 'myfile1'){
    if(type[1] == 'pdf' || type[1] == 'octet-stream')
      cb(null,true);
    else{
      console.log('wrongg');
      cb(null,false);
    }
  }
  else if(file.fieldname == 'myfile2'){
    if(type[0] == 'image')
          cb(null,true);
    else{
          console.log('wrongg');
          cb(null,false);
    }
  }
}

var mymulter = multer({ storage: storage});

//////////// upload db /////////////////
var cpupload = mymulter.fields([{name: 'myfile1', maxCount: 1},{name: 'myfile2', maxCount:1}]);

router.post('/formaction',cpupload,function(req,res,next){
  /*
  if(req.files['myfile1'] == undefined)
    res.send('select your notes wisely.Only pdf');
  else if(req.files['myfile2'] == undefined)
    res.send('select your \'notes image\' wisely.Only images');
  else
    {*/
  var note = new notescollection();

  var oname = req.files['myfile1'][0].originalname;
  var sname = req.files['myfile1'][0].filename;
  var mimetype = req.files['myfile1'][0].mimetype;
  var simage = req.files['myfile2'][0].filename;
  var category = req.body.category;
  var subcategory = req.body.subcategory;
  var description = req.body.description;
  var uploader = req.user._id;
  var uploaderName = req.user.facebook.name;
  note.oname = oname;
  note.sname = sname;
  note.type = mimetype;
  note.category = category;
  note.subcategory = subcategory;
  note.description = description;
  note.simage = simage;
  note.uploaderid = uploader;
  note.uploaderName = uploaderName;
  note.save(function(err) {
                            if (err){
                                console.log('Error in Saving notes: '+err);
                                throw err;
                            }
                            console.log('added to db');
                        });
  console.log('i m in upload post');
  console.log(req.files);
  res.redirect('viewnote/'+sname+'/uhi');
   //res.redirect(req.get('referer'));
 // res.end('yoyoyo the upload ends');
//}
});


///////////////// download ///////////////////////

router.get('/downloads/:type1?/:type2?/:name?/:sname?',function(req,res){
  var ftype = req.params.type2;
  var fname = req.params.name;
  var sname = req.params.sname;

    if(ftype == 'jpeg' || ftype == 'png')
      folder = 'images/'
    else if(ftype == 'pdf')
      folder = 'pdf/'
    else if(ftype == 'x-zip-compressed')
      folder = 'zip/'
    else
      folder = 'others/'
    if(process.env.OPENSHIFT_DATA_DIR){
      var loc = process.env.OPENSHIFT_DATA_DIR;
      var file = loc + folder + fname;
    }
    else{
    var file = __dirname + '/../uploads/' + folder + fname;
    }
    res.download(file,sname);
  //console.log(file);
 /* if(req.isAuthenticated()){
    res.download(file);
  }
  else
    res.send('login to continue');
  */
});


///////////////////// actions //////////////////

router.get('/addtostack/:notesname?/:noteoname?/:notesimage?',function(req,res,next){
  if(req.isAuthenticated()){
      var user = req.user._id;
      var notesname = req.params.notesname;
      var noteoname = req.params.noteoname;
      var notesimage = req.params.notesimage;
      console.log('i m in addnote');
      notestack.find({notesname : notesname, userid:user},{},function(e,docs){
        console.log('in notesttack found');
        if(docs[0] == undefined){
            console.log('in notesttack found in deadend');
            var notestackdb = new notestack();
              notestackdb.notesname = notesname;
              notestackdb.noteoname = noteoname;
              notestackdb.notesimage = notesimage;
              notestackdb.userid = user;

             notestackdb.save(function(err) {
                                        if (err){
                                            console.log('Error in Adding to NoteStack: '+err);
                                            throw err;
                                        }
                                        console.log('added to db');
                                    });
           }
           else{
                console.log(' ALREADY EXIST');
                //res.send('already exist');
                //res.redirect(req.get('referer'));
           }
      });
      res.redirect(req.get('referer'));
  }
  else
    res.send('Error');
});

/*router.get('/removenote/:noteid?',function(req,res,next){
  var noteid = req.params.noteid;
  if(noteid==undefined){
    res.send('error');
  }
  else{
    notescollection.remove({_id : noteid}).exec();
    res.send('note removed');
  }
});*/

router.get('/removefromstack/:noten?',function(req,res,next){
  if(req.params.noten == undefined){
    res.send('error');
  }
  else if(req.isAuthenticated()){
    var notename = req.params.noten;
    notestack.remove({_id : notename}).exec();
    res.redirect(req.get('referer'));
  }
  else
    res.send('login to continue');
});

router.post('/submitcomment/:notename?',function(req,res,next){
  var userid = req.user._id;
  var userName = req.user.facebook.name;
  var notename = req.params.notename;

  var commentsdb = new allcomments();
  commentsdb.comment = req.body.comment;
  commentsdb.noteid = notename;
  commentsdb.userid = userid;
  commentsdb.username = userName;

  commentsdb.save(function(err) {
                            if (err){
                                console.log('Error in Saving notes: '+err);
                                throw err;
                            }
                            console.log('added to db');
                            res.redirect(req.get('referer'));
                        });
  res.end;
});


///////////////////////////  views  //////////////////////////////

router.get('/adminlogin', function(req, res,next) {
    // Display the Login page with any flash message, if any
    //res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('adminlogin', {auth:false, title: 'ADMIN'});
});


router.get('/contact',function(req,res,next){
  if(req.isAuthenticated()){
    var data = req.user._id;
            res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            res.render('contact', {title : 'Contact', auth:true,user: req.user});
    }
    else{
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('contact', {title : 'Contact', auth:false});
      }
});

router.get('/notestack',function(req,res,next){
  if(req.isAuthenticated()){
    var data = req.user._id;
      notestack.find({userid:data},{},function(e,docs){
            res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            res.render('notestack', {title : 'Notestack','arr' : docs, auth:true,user: req.user});
        });
    }
    else{
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('notestack', {title : 'Notestack', auth:false});
      }

});

router.get('/nlist/:branch?/:subcat?', function(req, res){
  cat=req.params.subcat;
  branch=req.params.branch;
  info=[branch,cat];
  console.log(info);
  if(req.isAuthenticated()){
    notescollection.find({category:branch,subcategory:cat},function(e,docs){
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('nlist', {title : 'Notes List','arr' : docs, auth:true,user: req.user,'info':info});
      });
  }
  else{
  notescollection.find({category:branch,subcategory:cat},function(e,docs){
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('nlist', {title : 'Notes List','arr' : docs, auth:false,'info':info});
    });
  }
});


router.get('/viewnote/:which?/:uhi?',function(req,res){
  var note = req.params.which;
  if(req.isAuthenticated()){
    var user = req.user._id;
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    notescollection.find({sname:note},{},function(e,docs){
      allcomments.find({noteid : note},{},function(e,allcomments){
        notestack.find({notesname : note, userid:user},{},function(e,docs2){
          console.log('in notesttack found');
          if(docs2[0] == undefined){
              res.render('noteview', {'title' : 'Note', auth : true,'arr' : docs,'arr2' : docs2,'comments' : allcomments,'user':req.user, inStack:false});
          }
          else{
              res.render('noteview', {'title' : 'Note', auth : true,'arr' : docs,'arr2' : docs2,'comments' : allcomments,'user':req.user, inStack:true});
          }
        });

        });
      });
  }
  else{
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    notescollection.find({sname:note},{},function(e,docs){
      allcomments.find({noteid : note},{},function(e,allcomments){
            res.render('noteview', {'title' : 'Note', auth : false,'arr' : docs,'comments' : allcomments});
        });
    });
  }
});

router.get('/viewsubcat/:branch?',function(req,res){
  var branch = req.params.branch;
  if(req.isAuthenticated()){
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('topiclist', {'title' : branch + ' notes', 'branch' : branch, auth : true,'user':req.user});
  }
  else{
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('topiclist', {'title' : branch + ' notes', 'branch' : branch, auth : false});
  }
});

router.get('/viewsubbranch/:branch?',function(req,res){
  var branch = req.params.branch;
  if(req.isAuthenticated()){
      notescollection.find({category: branch},{},function(e,docs){
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('nlist', {'title' : branch + ' notes', arr : docs, auth : true,'user':req.user});
      });
  }
  else{
      notescollection.find({category: branch},{},function(e,docs){
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('nlist', {'title' : branch + ' notes', arr : docs, auth : false});
      });
  }
});

router.get('/upload',function(req,res){
  if(req.isAuthenticated()){
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('upload', {title : 'Upload', auth: true ,user: req.user});
  }
  else{
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('upload', {title : 'Upload', auth: false});
  }
});

router.get('/about',function(req,res){
  if(req.isAuthenticated()){
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('about', {title : 'About', auth: true ,user: req.user});
  }
  else{
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('about', {title : 'About', auth: false});
  }
        
});

router.get('/blog',function(req,res){
  if(req.isAuthenticated()){
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('blog', {title : 'Blog', auth: true ,user: req.user});
  }
  else{
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('blog', {title : 'Blog', auth: false});
  }
        
});
/*      AJAX POST ------ SIGNUP PAGE
    *   User name check availability ajax module
    */
/*    router.post('/checkUnameAvailable',function(req,res){
           console.log("request = "+req.body);
           var uName=req.body.usrname;
           console.log('user name = '+uName);
           var availability={
                   userName: uName,
                   status:{ connection_status:'ok',user_availability:'ok'},
                 };
         res.status(200).json(availability);
    });   */

/*    AJAX POST ------ COMMENT PAGE
router.post('/sendcomment',function(req,res){
          console.log('req = '+req.body.fname +' '+req.body.email );
          //res.json({a:d,g:j});
          var data={
                   userName: req.body.fname,
                   status:{ connection_status:'ok',user_availability:'ok'}
                 };
          res.status(200).json(data);

    });   */

module.exports = router;



//////////////  AUTHENTICATION  ////////////////


module.exports = function(passport){

//////////  fb login /////////////

router.get('/auth/facebook', passport.authenticate('fblogin', { scope : 'email' }));

    // handle the callback after facebook has authenticated the user
router.get('/auth/facebook/callback',
        passport.authenticate('fblogin', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));


	/* GET login page. */
	router.get('/login',isAuthenticated, function(req, res) {
    	// Display the Login page with any flash message, if any
		res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
		res.render('index', { message: req.flash('message'), user: req.user, auth:true, title: "Home"
  });
	});

  router.get('/redirect', function(req, res) {
      // Display the Login page with any flash message, if any
      res.redirect(req.headers.referer);
  /*  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('index', { message: req.flash('message'), user: req.user, auth:true, title: "Header"
  });*/
  });
   

	/* Handle Login POST */
	router.post('/login1', passport.authenticate('login', {
		successRedirect: '/',
		failureRedirect: '/login',
		failureFlash : true
	}));

	/* GET Registration Page */
  router.get('/signup', function(req, res){
    res.send("Signup will be back soon, guys...!!!");
  });
/*
	router.get('/signup', function(req, res){
    if(req.isAuthenticated()){
      res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
      res.render('index',{message: req.flash('message'), user: req.user, auth: true, title: "index",rec: recovery_options });
    }
    else{
		res.header('Cache-Control','no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('register',{message: req.flash('message'),auth: false, title: "register",rec: recovery_options});
  }
  });
*/

  router.get('/change-password', function(req,res){
    res.send('You don\'t have an account to recover.');
  /*  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render ('change-password',{auth: false, title: "change password",rec: recovery_options,message: req.flash('message')});
  */
});


  /*
  router.post('/change-password1', function(req,res){
    console.log(req.body.username);
    console.log(req.body.question);
    console.log(req.body.answer);
    user.findOne({username: req.body.username},function(err,doc){
      if(doc==null){
        req.flash('message','Incorrect username or answer');
        res.redirect('/change-password');
      }
      else{
        if((doc.passRecIndex==req.body.question)&&(doc.PassRecAnswer==req.body.answer)){
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
          res.render('new-password',{auth: true, title: "new-password" , user: doc});
        }
        else{
          req.flash('message','Incorrect username or answer');
          res.redirect('/change-password');
        }

      }
    });



  });
  router.get('/new-password', isAuthenticated,function(req,res){
    res.header('Cache-Control','no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('new-password',{message: req.flash('message'),auth: true, title: "new-password", user: req.user});
  });

  router.post('/new-password1',function(req,res){
        user.findOne({username: req.body.username},function(err,docs){
          if(docs==null){
              console.log("error")
          }
          else{
              var newPassword=bCrypt.hashSync(req.body.password, bCrypt.genSaltSync(10), null);
              user.update({username: req.body.username},{$set: {password: newPassword} },function(err,doc){
                console.log(req.body.password);
                console.log(newPassword);
                console.log(doc);
                req.logout();
            		res.redirect('/login');
              });
          }

        })

  });
*/

	/* Handle Registration POST */
  /*
	router.post('/signup', passport.authenticate('signup', {
		successRedirect: '/profile-set',
		failureRedirect: '/signup',
		failureFlash : true
	}));

*/
  ////////////////////// GET profile set\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  router.get('/profile-set',isAuthenticated,function(req,res){
    res.header('Cache-Control','no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('profile-set',{message: req.flash('message'),auth: true,user: req.user,title: "profile-set"});
  });



    ///////////// storage for profile-pic \\\\\\\\\\\\\\\\\\\\\\\\
  var storagePic = multer.diskStorage({
    destination: function (req, file, cb) {
      console.log('im here ');
        cb(null, __dirname+'/../uploads/profilePic');
        },

    filename: function (req, file, cb) {
      var ext = file.originalname.split(".");
      cb(null, 'a' + Date.now() +'.' +ext[1] )
    }
  });
  
  var filefilter = function(req,file,cb){
    var type = file.mimetype.split('/');
      if(type[0] == 'image')
        cb(null,true);
      else{
        console.log('wrongg');
        cb(null,false);
      }
  }
  var mymulterPic=multer({storage: storagePic,fileFilter: filefilter})


 /* router.get('/upload2', function(req, res, next) {
    res.render('upload2', { title: 'Upload' });
  });*/

  router.get('/profile',isAuthenticated,function(req,res){
   //res.header('Cache-Control','no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
   var user = req.user._id;
    notescollection.find({uploaderid: user},{},function(e,docs){
      console.log(docs + 'hi');
          res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('profile', {'title' : 'Profile', arr : docs, auth : true,'user':req.user,message: req.flash('message')});
    });
  });


  router.post('/profile-set1',mymulterPic.single('profile_pic'),function(req,res){
          if(req.file == undefined){
            res.send('u don\'t look good');
          }
          else{
              var filename=req.file.filename;
              var usernm=req.body.userj;
              console.log(usernm);
              console.log(filename);
              user.findOne({username: usernm},{_id:1},function(err,docs){
                if(err){
                  console.log(err);
                }
                else{
                    user.update({_id: docs},{profilePic: filename},function(err,user1){
                      if(err){
                        console.log(err);
                    }
                      else{
                          console.log("profile pic inserted");
                          console.log(user1);
                          res.redirect('profile');
                          //res.render('profile',{message: req.flash('message'),auth: true,user: req.user,title: "Home"});
                      }
                    });
                  }
            });
          }
});

	/* Handle Logout */
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/login');
	});

	return router;
}
